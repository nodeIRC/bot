/**
 * Created by Platzhalter on 05.01.2016
 */
module.exports = exports = function (lstnrs, config) {
    /**
     * config:
     * {
     *    "server": "212.227.159.137",
     *    "password": null,
     *    "nickName": "Bot",
     *    "userName": "Bot",
     *    "realName": "Bot",
     *    "port": "6667",
     *    "autoRejoin": true,
     *    "autoConnect": true,
     *    "channels": [ "#test" ],
     *    "ident": "bot@bot",
     *    "commandChar": "!",
     *    "operPassword": "password"
     *  }
     */

    var listeners = {
        "command": {
            DEFAULT: function (client, sender, channel, command) {
                this.say(channel, "The command \"" + command + "\" does not exist.");
            }
        },
        "message": function (client, sender, channel, message) {
            console.log(sender + " sent \"" + message + "\" on " + channel);
        },
        "pm": function (client, sender, message, trusted) {
            console.log(sender + (trusted ? " [*]" : "") + " pmd you \"" + message + "\"");
        },
        "notice": function(client, from, text, message) {
            console.log("Got this notice from " + from + ": " + text + " with message: " + message)
        }
    };

    for(var c in lstnrs) if(lstnrs.hasOwnProperty(c)) listeners[c] = lstnrs[c];

    return require('./lib/client.js')(listeners, config);
};